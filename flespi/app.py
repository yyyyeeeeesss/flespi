from aiohttp import web
from .api import listenMqtt

async def create_app():
    app = web.Application()
    listenMqtt(app)
    return app